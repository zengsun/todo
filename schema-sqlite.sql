-- mysql schema.sql
CREATE TABLE user(
    id INTEGER PRIMARY KEY AUTOINCREMENT ,
    username VARCHAR (16) NOT NULL ,
    password VARCHAR (128) NOT NULL ,
    nickname VARCHAR (50) NOT NULL ,
    active BOOLEAN DEFAULT 1,
    superuser BOOLEAN DEFAULT 0
);

CREATE UNIQUE INDEX idx_user_username ON user (username);

CREATE TABLE todo(
    id INT PRIMARY KEY AUTO_INCREMENT ,
    title VARCHAR (100) NOT NULL ,
    description VARCHAR (255) NOT NULL ,
    priority VARCHAR (25) NOT NULL ,
    finished_at DATETIME ,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    user_id INT not NULL,
    FOREIGN key (user_id) REFERENCES user (id)
);



SELECT '全部' as class, subject, max(score), min(score), avg(score) FROM score
GROUP BY subject
UNION all;
SELECT class, subject, max(score), min(score), avg(score) FROM score
WHERE year = 2021
GROUP BY class, subject;

SELECT student, subject, max(score), min(score), avg(score) FROM score
GROUP BY student, subject;

insert into score (class, student, subject, score, year)
select a.* , floor(rand() * 100) as score, 2019 as year
from (SELECT distinct class, student, subject FROM score) as a;


ALTER TABLE score ADD COLUMN year int;

SELECT * from score WHERE student = '张良' and subject = 'physics';
update score set year = 2020;

DELETE FROM  score where year = 2019;

INSERT INTO score (class, student, subject, score)
VALUES
('web04', '张良', 'math', 80),
('web04', '张良', 'lang', 40),
('web04', '张良', 'history', 60),
('web04', '张良', 'physics', 60),
('web04', '李斯', 'math', 40),
('web04', '李斯', 'lang', 90),
('web04', '李斯', 'history', 40),
('web04', '李斯', 'physics', 20),
('web04', '赵高', 'math', 20),
('web04', '赵高', 'lang', 99),
('web04', '赵高', 'history', 9),
('web04', '赵高', 'physics', 10),
('web04', '胡亥', 'math', 20),
('web04', '胡亥', 'lang', 29),
('web04', '胡亥', 'history', 9),
('web04', '胡亥', 'physics', 10),
('web05', '白起', 'math', 70),
('web05', '白起', 'lang', 80),
('web05', '白起', 'history', 70),
('web05', '白起', 'physics', 40),
('web05', '李牧', 'math', 90),
('web05', '李牧', 'lang', 90),
('web05', '李牧', 'history', 70),
('web05', '李牧', 'physics', 80),
('web05', '王翦', 'math', 90),
('web05', '王翦', 'lang', 99),
('web05', '王翦', 'history', 89),
('web05', '王翦', 'physics', 70),
('web05', '廉颇', 'math', 72),
('web05', '廉颇', 'lang', 89),
('web05', '廉颇', 'history', 79),
('web05', '廉颇', 'physics', 80);