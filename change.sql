-- 将 description 扩容到 1000
ALTER TABLE todo modify COLUMN description varchar(1000);

-- 2021-6-2
ALTER TABLE USER
  ADD COLUMN avatar varchar(255);

