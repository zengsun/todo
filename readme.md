# 简单的 Todo 列表应用

*待办列表应用*是大家较容易理解，也比较常见的应用。
由于业务逻辑较为简单，非常适合入门教学的案例使用。
本示例旨在为中兴软件培训的同学提供一个服务端的最佳实践参考。

在本示例中提供如下实践参考：

1. express 框架的基本使用
   1. 定义路由、静态文件服务
   2. 使用会话对象 Session 实现用户状态的保存
   3. 使用自定义 express 中间件实现对用户登录状态的统一验证
   4. 使用 express-validator
2. ORM 工具的使用，自动同步数据结构

本示例实现以下路由：

3. 用户注册：
   /register,
   POST,
   {username, password, nickname},
   {success: true | false, message: ""}
4. 登录：
   /login
   POST
   {username, password}
   {success: true, }
5. 登出:
   /logout
   GET
   {success: true}
6. 获取登录用户所有待办：
   /todo,
   GET,
   {success: true | false, query:[]}

7. 添加新待办：
   /add/todo
   POST
   {title, description, priority}
   {success: true | false, object: {}}

8. 删除待办：
   /delete/todo/{tid}
   DELETE
   {success: true | false}

9. 完成待办:
   /finished/todo/{tid}
   PUT
   {success: true | false}

10. 获取用户信息
    /user/info
    GET
    {success: true | false, object: {user}}

11. 获得统计信息
    /user/stat
    GET
    {success: Boolean, object: {}}
